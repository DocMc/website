import './App.css';

import React from 'react'

import { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";

import Banner from "./components/Banner";
import Header from "./components/Nav/Header";
import Search from "./components/Search/Search";
import Home from './pages/Home/Home';
import Dropdown from "./components/Dropdown/Dropdown";
import Seasons from "./components/Seasons/Seasons";
import Prime from "./components/Prime/Prime";
import { useCollection } from "./hooks/useCollection";
import Login from './pages/Login/Login';
import { useAuthContext } from "./hooks/useAuthContext";

const App = () => {
    const { documents: options } = useCollection("dropDownOptions");
    const [selectedOption, setSelectedOption] = useState(null);
    const { user } = useAuthContext();

    useEffect(() => {
        if (options) {
            setSelectedOption(options[0]);
        }
    }, [options]);

    return (
        <div className='container'>
            <Router>
                <div className="top-card">
                    <Banner
                        title="Jon McCourry"
                        meta=""
                        text="Welcome to my personal project page. My name is Jon McCourry and I am hoping on becoming a junior developer. Here are some widgets I have put together."
                    />
                </div>
                <Header />
                <hr className='line'/>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path='/login' element={(!user) ? <Login/> : <Navigate to='/'/>}/>
                    <Route path="/wikisearch" element={<Search />} />
                    <Route
                        path="/dropdown"
                        element={
                            selectedOption && (
                                <>
                                    <Dropdown
                                        options={options}
                                        label="Select a Color"
                                        selected={selectedOption}
                                        onSelectedChange={setSelectedOption}
                                    />
                                    <div
                                        className="ui "
                                        style={{ color: selectedOption.value }}
                                    >
                                        This Text Changes Colors
                                    </div>
                                </>
                            )
                        }
                    />

                    <Route path="/seasons" element={<Seasons />} />
                    <Route path="/prime" element={<Prime />} />
                </Routes>
            </Router>
        </div>
    );
};

export default App;
