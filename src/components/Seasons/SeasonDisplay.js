import React from 'react';
import PropTypes from 'prop-types';
import './SeasonDisplay.css';

const seasonConfig = {
    summer: {
        text: 'Lets hit the beach',
        iconName: 'sun'
    },
    winter: {
        text: 'Burr, it is cold',
        iconName: 'snowflake'
    }
}
const getSeason = (lat, month) => {
    if (month > 2 && month < 9){
        return lat > 0 ? 'summer': 'winter';
    }
    else{
        return lat > 0 ? 'winter' : 'summer';
    }
}

const SeasonDisplay = ({lat}) => { 
    const season = getSeason(lat, new Date().getMonth());
    const {text, iconName} = seasonConfig[season];
    return(
        <div data-testid='season-display' className={`season-display ${season} column gid`}>
            <i className={`${iconName} icon huge icon-left`} />
            <i className={`${iconName} icon huge icon-top-right`} />
            <h1>{text}</h1>
            <i className={`${iconName} icon huge icon-right`}  />
            <i className={`${iconName} icon huge icon-bottom-left`}  />
        </div>
    )
}

SeasonDisplay.propTypes = {
    lat: PropTypes.number,
}

export default SeasonDisplay;