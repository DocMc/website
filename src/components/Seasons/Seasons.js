import React, {useState, useEffect} from 'react';
import SeasonDisplay from './SeasonDisplay';
import Spinner from '../Spinner';

const Seasons = () => {
    const [lat, setLat] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        window.navigator.geolocation.getCurrentPosition(
            (pos) => setLat(pos.coords.latitude),
            (err) => setError(err.message)
        )
    }, []);

    const render = () => {
        if(error && !lat){
            return <div>Error: {error} </div>
        }
        if(!error && lat){
            return <SeasonDisplay lat={lat} />
        }
        return <div><Spinner text="Waiting for Location request"/></div>
    }
    return (
        <div data-testid='Seasons' >
            {render()}
        </div>
    )
}

export default Seasons;