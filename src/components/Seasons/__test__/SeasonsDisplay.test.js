import React from 'react';
import ReactDom from 'react-dom';
import SeasonDisplay from '../SeasonDisplay';

import {render} from '@testing-library/react';
import '@testing-library/jest-dom';
import * as renderer from 'react-test-renderer'

it('SeasonDisplay rendered', () => {
    const div = document.createElement('div');
    ReactDom.render(<SeasonDisplay/>, div);
});

it('SeasonDisplay works', () => {
    const {getByTestId} = render(<SeasonDisplay/>);
    expect(getByTestId('season-display')).toHaveTextContent("Burr, it is cold");
});

it('snapshot matches', () => {
    const tree = renderer.create(<SeasonDisplay/>).toJSON();
    expect(tree).toMatchSnapshot();
})