import React from 'react';
import ReactDOM from 'react-dom';
import Seasons from '../Seasons';

import * as renderer from 'react-test-renderer';

import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

it('renders Seasons correctly', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Seasons/>, div);
})
const mockGeolocation = {
    getCurrentPosition: jest.fn()
    .mockImplementationOnce((success, failure, options) => Promise.resolve(success({
        coords: {
            latitude: null,
            longitude: null
        }
    })))
}

global.navigator.geolocation = mockGeolocation;

it('Seasons works', () => {
    const snap = renderer.create(<Seasons/>)
    expect(snap).toMatchSnapshot();
})

it('SeasonDisplay as child', () => {
    const {getByTestId} = render(<Seasons/>);
    expect(getByTestId('Seasons')).toHaveTextContent('Waiting for Location request') 
});