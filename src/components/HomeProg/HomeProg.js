import React from 'react';
import './HomeProg.css';
import { useCollection } from '../../hooks/useCollection';

const HomeProg = ({ header }) => {
    const { documents: langs } = useCollection('progSkills', null, ['order'])

    const renderedLangs = () => {
        return langs.map((lang) => {
            return (
                <div className='prog-card' key={lang.lang}>
                    <img alt={`${lang.lang}`}  src={`/assets/${lang.logo}`} />
                    <ul className='prog-item header'>
                        {lang.lang}
                    </ul>
                </div>
            )
        })
    }

    return (
        <div className='program-list'>
            <h1 className='title'>{header}</h1>
            <div className='prog-container'>
                {langs && renderedLangs()}
                <p>*Site was built with React</p>
            </div>
        </div>
    )
}

export default HomeProg;