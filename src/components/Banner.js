import React from 'react';

const Banner = ({ title, meta, text }) => {
    return (
            <div 
                className='ui top attached center aligned header banner'
            >
                <h1>
                {title}</h1>
                <div className='meta'>
                    <h3>{meta}</h3>
                </div>
                <div className='description'>
                    {text}
                </div>
                <div> 
                    <a href={`mailto: jon.mccourry@gmail.com`}>Contact Me</a>
                </div>
            </div>
    )
}
export default Banner;