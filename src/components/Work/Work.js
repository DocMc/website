import React from 'react';
import style from  './Work.module.css';
import { useCollection } from '../../hooks/useCollection';

const Work = ({ collection, header, orderBy }) => {
    const { documents: places } = useCollection(collection, null, orderBy);

    const renderedDists = (dists) => {
        return dists.map((dist) => {
            return (
                <li className={style['list-item']} key={dist}>{dist}
                </li>
            )
        })

    }

    const renderedPlaces = () => {
        return places.map((place) => {
            return (
                <div key={place.title} className='work-div'>
                    <div  className='basic-card'>
                        <div className='header'>
                            <img alt={`${place.logo ? place.place : ''}`} src={`/assets/${place.logo}`} />
                            <div className='content title'>
                                {place.place}
                            </div>
                        </div>
                        <div>
                                <ul>
                                    <li className='header'>{place.title}</li>
                                    <ul>
                                        {place.dists ? renderedDists(place.dists) : ''}
                                    </ul>
                                </ul>
                            </div>
                    </div>
                </div>
            )
        })
    }

    return (
        <div >
            <h1 className='title'>{header}</h1>
            {places && renderedPlaces()}
        </div>
    )
}

Work.defaultProps = {
    header: '',
}

export default Work;