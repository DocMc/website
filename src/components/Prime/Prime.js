import React, { useState } from "react";

const Prime = () => {
    const [numberToFactor, setNumberToFactor] = useState(0);
    const [factors, setFactors] = useState([]);
    const [powersOfFactors, setPowersOfFactors] = useState([]);

    const primeFactors = (num) => {
        const isPrime = (num) => {
            for (let i = 2; i <= Math.sqrt(num); i++) {
                if (num % i === 0) return false;
            }
            return true;
        }
        const result = [];
        for (let i = 2; i <= num; i++) {
            while (isPrime(i) && num % i === 0) {
                result.push(i);
                num /= i;
            }
        }
        let powers = [];
        let f = [...result];
        let count = 0;
        for (let i = 0; i < f.length; i++) {
            if (f[i] !== f[i + 1]) {
                for (let j = 0; j < f.length; j++) {
                    if (f[i] === f[j]) {
                        count++;
                    }
                }
            }
            if(count !== 0) powers.push(count);
            count = 0;
        }
        let uf = [];
        f.forEach((c) => {
            if(!uf.includes(c)){
                uf.push(c);
            }
        })
        setFactors(uf);
        setPowersOfFactors(powers);
    }

    const renderedfactors = () => {
        return factors.map((factor, index) => {
            return (
                <div key={factor} style={{ display: 'inline' }}>
                    <span>{` ${factor}^${powersOfFactors[index]} `}, </span>
                </div>
            )
        })
    }


    return (
        <div>
            <label className='label'>Enter Number</label>
            <div className='ui action input prim-input'>
                <input
                    type="number"
                    value={numberToFactor}
                    onChange={e => {if(numberToFactor < 1000000 || numberToFactor > e.target.value) setNumberToFactor(e.target.value)}}
                />
                <button className='ui button' onClick={() => primeFactors(numberToFactor)}>Factor</button>
            </div>
            <div>
                {renderedfactors()}
            </div>
        </div>
    )
}

export default Prime;