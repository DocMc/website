import { useCollection } from "../../hooks/useCollection"
import React from 'react';
const HomeGit = () => {
    const { documents: gits } = useCollection('gitLab');

    const renderedGits = () => {
        return gits.map((git) => {
            return (
                <div className='basic-card git' key={git.site}>
                    <div className='ui header'>
                    <div className='content title'>{git.site}</div>
                    <ul>
                        <li>
                            <a href={git.url}>{git.url}</a>
                        </li>
                    </ul>
                    </div>
                </div>
            )
        })
    }

    return (
        <div>
            <h1 className='title'>Git:</h1>
            {gits && renderedGits()}
            <br/>
        </div>
    )
}

export default HomeGit;