import React, {useState} from 'react';
import ReactDom from 'react-dom';
import {isTSAnyKeyword} from '@babel/types';
import Dropdown from '../Dropdown';
import {options} from '../../../constance';

import * as renderer from 'react-test-renderer';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom';



it("renders without crashing",() => {
    const div = document.createElement('div');
    ReactDom.render(<Dropdown/>, div)
});

it("renders Dropdown correctly", () =>{
    const selectedOption = options[0];
    const setSelecteOption = (pass) => {
        selectedOption = pass;
    }
    const {getByTestId} = render(<Dropdown
            options={options}
            selected={selectedOption}
            onSelectedChange={setSelecteOption}
            label="stuff"
            />)
    for(let i = 0; i < options.length; i++){
        expect(getByTestId('dropdown')).toHaveTextContent(options[i].label)
    }
});

it('match snapshot', () => {
    const selectedOption = options[0];
    const setSelecteOption = (pass) => {
        selectedOption = pass;
    }
    const snap = renderer.create(<Dropdown
        options={options}
        selected={selectedOption}
        onSelectedChange={setSelecteOption}
        label="stuff"
        />).toJSON()
    expect(snap).toMatchSnapshot();
});