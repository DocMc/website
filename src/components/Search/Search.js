import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types'
import wiki from '../../apis/wiki';
import Spinner from '../Spinner';

const Search = ({ firstTerm }) => {
    firstTerm = (sessionStorage.getItem('search') ? sessionStorage.getItem('search') : firstTerm);
    const [term, setTerm] = useState(firstTerm);
    const [results, setResults] = useState([]);
    const [isSearching, setIsSearching] = useState(false);

    const callBack = async (term) => {
        const { data } = await wiki.get('', {
            params: {
                srsearch: term
            }
        });
        setIsSearching(false);
        setResults(data.query.search);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        callBack(term);
    }


    const renderedResults = results.map((result) => {
        return (
            <div key={result.pageid} className="item">
                <div className='right floated content'>
                    <a
                        className="ui button"
                        href={`https://en.wikipedia.org?curid=${result.pageid}`}
                    >Go
                    </a>
                </div>
                <div className="content">
                    <div className='header'>{result.title}</div>
                    <span dangerouslySetInnerHTML={{ __html: result.snippet }}></span>
                </div>
            </div>
        )
    })
    
    return (
        <div>
            <form onSubmit={handleSubmit} className="ui form">
                <div className="field">
                    <label>Enter Search Term and Press Enter
                    <input
                        type='text'
                        className="ui right labeled input"
                        value={term}
                        onChange={(e) => {!isSearching && setTerm(e.target.value)}}
                    />
                    <button className='ui rigtht floated button' onClick={() => callBack(term)}>Search!</button>
                    </label>
                </div>
            </form>
            {renderedResults.length === 0 ? 'Please Type in Search Term' : ''}
            <div className={`ui celled list ${(renderedResults.length === 0) ? "" : "segment"}`}>
                {isSearching && <Spinner text="getting results" />}
                {renderedResults}
            </div>
        </div>
    )
}
Search.defaultProps = {
    firstTerm: ''
}

Search.propsType = {
    firstTerm: PropTypes.string,
}

export default Search;