//styles
import './Education.css';

import React from 'react';
import { useAuthContext } from "../../hooks/useAuthContext";
import { useCollection } from "../../hooks/useCollection";
import { useFirestore } from "../../hooks/useFirestore";

const Education = ({ header }) => {
    const collection = "education";
    const { documents: places } = useCollection(collection);
    const { user } = useAuthContext();
    const { addDocument, deleteDocument } = useFirestore(collection);

    const renderedDists = (dists) => {
        return dists.map((dist) => {
            return (
                <li key={dist}>
                    <p>{dist}</p>
                </li>
            );
        });
    };

    const handleDelete = (id) => {
        deleteDocument(id);
    }

    const renderedPlaces = () => {
        return places.map((place) => {
            return (
                <div key={place.duration} className="education-div">
                    <div className="basic-card">
                        <div className="education">
                            <img
                                alt={place.place}
                                src={`./assets/${place.logo}.jpeg`}
                            />
                            <div className="header">{place.place}</div>
                            <div className="description para">
                                <ul>
                                    <li className="list">{place.duration}</li>
                                    <ul>
                                        {place.dists
                                            ? renderedDists(place.dists)
                                            : ""}
                                    </ul>
                                </ul>
                            </div>
                            {user && (
                                    <button onClick={() => handleDelete(place.id)} className="btn delete" style={{paddingRight: '15px', position: 'absolute', justifyContent: 'right'}}>
                                        Delete
                                    </button>
                                )}
                        </div>
                    </div>
                </div>
            );
        });
    };

    return (
        <div>
            <h1 className="title">{header}</h1>
            {places && renderedPlaces()}
        </div>
    );
};

Education.defaultProps = {
    header: "",
};

export default Education;
