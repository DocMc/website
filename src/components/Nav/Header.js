import React from 'react';
import Sites from "../../constance/Sites.js";
import './Nav.css';
import { NavLink } from 'react-router-dom';
import { useAuthContext } from "../../hooks/useAuthContext.js";
import { useLogout } from '../../hooks/useLogout';

const Header = () => {
    const { user } = useAuthContext();
    const {logout} = useLogout();
    
    const renderedLinks = Sites.map((link) => {
        return (
            <NavLink key={link.href} to={link.href} className='item'>
                {link.title}
            </NavLink>
        )
    })

        return (
            <div className='ui secondary pointing menu nav-bar'>
                    {renderedLinks}
                    {user && (
                        <div className='item' onClick={logout}>Logout</div>
                    )}
            </div>
        )
}

export default Header;