import {initializeApp} from 'firebase/app';
import { getFirestore, Timestamp } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyAVZDAxTE7F5ZEuxX7e6O8hkUY12UjT8KU",
    authDomain: "docmccourry.firebaseapp.com",
    projectId: "docmccourry",
    storageBucket: "docmccourry.appspot.com",
    messagingSenderId: "887437808506",
    appId: "1:887437808506:web:f7ee8d0b1677ea6526bcc3",
};

const firebaseApp = initializeApp(firebaseConfig);

const projectFirestore = getFirestore(firebaseApp);
const projectAuth = getAuth(firebaseApp);

const timeStamp = Timestamp.fromDate(new Date());

export { projectAuth, projectFirestore, timeStamp }
