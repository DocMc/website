import { useState, useEffect } from "react";
import { projectAuth } from "../firebase/config";
import { useAuthContext } from "./useAuthContext";
import { signInWithEmailAndPassword } from "firebase/auth";

export const useLogin = () => {
    const [isCanclled, setIsCancelled] = useState(false);
    const [error, setError] = useState(null);
    const [isPending, setIsPending] = useState(false);
    const { dispatch } = useAuthContext();

    const login = async (email, password) => {
        setError(null);
        setIsPending(true);

        //sign the user out
        try {
            let { user } = await signInWithEmailAndPassword(projectAuth, email, password);

            //dispatch logout action
            dispatch({ type: "LOGIN", payload: user });

            //update state
            if (!isCanclled) {
                setIsPending(false);
                setError(null);
            }
        } catch (err) {
            if (!isCanclled) {
                console.log(err.message);
                setError(err.message);
                setIsPending(false);
            }
        }
    };

    

    useEffect(() => {
        return () => setIsCancelled(true);
    }, []);

    return { error, isPending, login };
    
};
