import { useReducer, useState, useEffect } from "react";
import { projectFirestore, timeStamp } from "../firebase/config";
import { addDoc, collection, deleteDoc, doc } from "firebase/firestore";

let initialState = {
    document: null,
    isPending: false,
    error: null,
    success: null,
};

const firestoreReducer = (state, action) => {
    switch (action.type) {
        case "IS_PENDING":
            return {
                isPending: true,
                document: null,
                success: null,
                error: null,
            };
        case "GET":
            return {
                isPending: false,
                document: action.payload,
                success: true,
                error: null,
            };
        case 'DELETE':
            return {
                isPending: false,
                document: null,
                success: true,
                error: null,
            }
        case "ERROR":
            return {
                isPending: false,
                document: null,
                success: false,
                error: action.payload,
            };
        default:
            return state;
    }
};

export const useFirestore = (col) => {
    const [response, dispatch] = useReducer(firestoreReducer, initialState);
    const [isCancelled, setIsCancelled] = useState(false);

    //only dispatch only if not cancelled
    const dispatchIfNotCancelled = (action) => {
        if (isCancelled === false) {
            dispatch(action);
        }
    };

    const ref = collection(projectFirestore, col);
    //add a document
    const addDocument = async (data) => {
        dispatch({ type: "IS_PENDING" });

        try {
            let newData = {...data, createdAt: timeStamp}
            const addedDocument = await addDoc(ref, newData);
            dispatchIfNotCancelled({ type: "GET", payload: addedDocument });
        } catch (err) {
            dispatchIfNotCancelled({ type: "ERROR", payload: err.message });
        }
    };

    // delete a document
    const deleteDocument = async (id) => {
        dispatch({type: 'IS_PENDING'});
        try{
            let document = doc(projectFirestore, col, id);
            const deletedDoc = await deleteDoc(document);
            dispatchIfNotCancelled({type: 'DELETE', payload: deletedDoc})
        }
        catch (err) {
            console.log(err);
            dispatchIfNotCancelled({type: 'ERROR', payload: 'Could not delete' })
        }
    };

    useEffect(() => {
        return () => setIsCancelled(true);
    }, []);

    return { addDocument, deleteDocument, response };
};
