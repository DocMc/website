import { useEffect, useState, useRef } from "react";
import { projectFirestore } from "../firebase/config";
import { collection, onSnapshot, query, where, orderBy} from "@firebase/firestore";

export const useCollection = (col, _queryArray, _orderBy) => {
    const [documents, setDocuments] = useState(null);
    const [error, setError] = useState(null);

    const queryArray = useRef(_queryArray).current;
    const ob = useRef(_orderBy).current;

    useEffect(() => {
        let ref = collection(projectFirestore, col);

        if(queryArray){
            ref = query(ref, where(...queryArray));
        }
        if(ob){
            ref = query(ref, orderBy(...ob));
        }

        const unsub = onSnapshot(ref, (snapshot) => {
            let results = [];
            snapshot.docs.forEach(doc => {
                results.push({...doc.data(), id: doc.id});
            })
        //update state
        setDocuments(results);
        //setError(null);
        }, (err) => {
            console.log(err);
            setError('could not fetch the data');
        });

        return () => unsub();

    },[col, queryArray, ob])

    return { documents, error }
}