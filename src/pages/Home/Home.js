import React from "react";
import "./Home.css";
import Work from '../../components/Work/Work';
import HomeGit from "../../components/HomeGit/HomeGit";
import HomeProg from "../../components/HomeProg/HomeProg";
import Education from "../../components/Education/Education";

const Home = () => {
    return (
        <div>
            <div className="my-container">
                <div className="edu">
                    <Education header=" Education:" />
                </div>
                <div className="">
                    <HomeGit/>
                </div>
            </div>
            <hr className="line" />
            <div className="my-container">
                <div className="work">
                    <Work collection='workDetails' header="Job Experience:" orderBy={['place', 'desc']} />
                </div>
                <div className="">
                    <HomeProg
                        header="Programming Languages"
                    />
                </div>
            </div>
            <hr className="line" />
            <div className="my-container">
                <div className="awards">
                    <Work collection='awards' header="Awards" orderBy={['place', 'desc']} />
                </div>
            </div>
        </div>
    );
};

export default Home;
